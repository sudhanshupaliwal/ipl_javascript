const MATCH_ID = 0;
const MATCH_SEASON = 1;
const MATCH_CITY = 2;
const MATCH_DATE = 3;
const MATCH_TEAM1 = 4;
const MATCH_TEAM2 = 5;
const MATCH_TOSS_WINNER = 6;
const MATCH_TOSS_DECISION = 7;
const MATCH_RESULT = 8;
const MATCH_DL_APPLIED = 9;
const MATCH_WINNER = 10;
const MATCH_WIN_BY_RUN = 11;
const MATCH_WIN_BY_WICKET = 12;
const MATCH_PLAYER_OF_MATCH = 13;
const MATCH_VENUE = 14;
const MATCH_UMPIRE1 = 15;
const MATCH_UMPIRE2 = 16;
const MATCH_UMPIRE3 = 17;

const DELIVERY_MATCH_ID = 0;
const DELIVERY_INNING = 1;
const DELIVERY_BATTING_TEAM = 2;
const DELIVERY_BOWLING_TEAM = 3;
const DELIVERY_OVER = 4;
const DELIVERY_BALL = 5;
const DELIVERY_BATSMAN = 6;
const DELIVERY_NON_STRIKER = 7;
const DELIVERY_BOWLER = 8;
const DELIVERY_IS_SUPER_OVER = 9;
const DELIVERY_WIDE_RUN = 10;
const DELIVERY_BYE_RUN = 11;
const DELIVERY_LEG_BYE_RUN = 12;
const DELIVERY_NO_BALL_RUN = 13;
const DELIVERY_PENALTY_RUN = 14;
const DELIVERY_BATSMAN_RUN = 15;
const DELIVERY_EXTRA_RUN = 16;
const DELIVERY_TOTAL_RUN = 17;
const DELIVERY_PLAYER_DISMISSED = 18;
const DELIVERY_DISMISSAL_KIND = 19;
const DELIVERY_FIELDER = 20;

var fs = require('fs');

var matchesData = fs.readFileSync('/home/sudhanshu/Downloads/matches.csv', 'utf-8');
var matchesCsvAsArray = matchesData.split(/\r?\n/);
const matches = [];
matchesCsvAsArray.forEach(function (row) {
    let match = row.split(',');
    matches.push(match);
});

var deliveriesData = fs.readFileSync('/home/sudhanshu/Downloads/deliveries.csv', 'utf-8');
var deliveriesCsvAsArray = deliveriesData.split(/\r?\n/);
const deliveries = [];
deliveriesCsvAsArray.forEach(function (row) {
    let delivery = row.split(',');
    deliveries.push(delivery);
});

var findMatchesPlayedPerYear = function (matches) {
    let matchesPlayedPerYear = {};
   for(let i = 1;i<matches.length;i++){
       let match = matches[i];
       if (match[MATCH_SEASON] in matchesPlayedPerYear) {
        matchesPlayedPerYear[match[MATCH_SEASON]] += 1;
    }
    else {
        matchesPlayedPerYear[match[MATCH_SEASON]] = 1;
    }
   }
    delete matchesPlayedPerYear["undefined"];
    console.log(matchesPlayedPerYear);
}

var findTotalMatchesWonByEachTeam = function (matches) {
    let totalMatchesWonByEachTeam = {};
    for(let i = 1;i<matches.length;i++){
        let match = matches[i];
        if (match[MATCH_WINNER] in totalMatchesWonByEachTeam) {
            totalMatchesWonByEachTeam[match[MATCH_WINNER]] += 1;
        }
        else {
            totalMatchesWonByEachTeam[match[MATCH_WINNER]] = 1;
        }
    }
    delete totalMatchesWonByEachTeam["undefined"];

    console.log(totalMatchesWonByEachTeam);
}

var findExtraRunConcededPerTeamIn2016 = function (matches, deliveries) {
    let extraRunConcededperteamIn2016 = {};
    const matchIdIn2016 = new Set();
    matches.forEach(function (match) {
        if (match[MATCH_SEASON] == 2016) {
            matchIdIn2016.add(match[MATCH_ID]);
        }
    });
    deliveries.forEach(function (delivery) {
        if (matchIdIn2016.has(delivery[DELIVERY_MATCH_ID])) {
            if (delivery[DELIVERY_BOWLING_TEAM] in extraRunConcededperteamIn2016) {
                extraRunConcededperteamIn2016[delivery[DELIVERY_BOWLING_TEAM]] += Number(delivery[DELIVERY_EXTRA_RUN]);
            }
            else {
                extraRunConcededperteamIn2016[delivery[DELIVERY_BOWLING_TEAM]] = Number(delivery[DELIVERY_EXTRA_RUN]);
            }
        }
    });
    console.log(extraRunConcededperteamIn2016);
}

var findSortedBowlerEconomyIn2015 = function (matches, deliveries) {
    let bowlerData = {};
    const matchIdIn2015 = new Set();
    matches.forEach(function (match) {
        if (match[MATCH_SEASON] == 2015) {
            matchIdIn2015.add(match[MATCH_ID]);
        }
    });
    deliveries.forEach(function (delivery) {
        if (matchIdIn2015.has(delivery[DELIVERY_MATCH_ID])) {
            if (delivery[DELIVERY_BOWLER] in bowlerData) {
                bowlerData[delivery[DELIVERY_BOWLER]][0] += 1;
                bowlerData[delivery[DELIVERY_BOWLER]][1] += Number(delivery[DELIVERY_WIDE_RUN]) + Number(delivery[DELIVERY_NO_BALL_RUN]) + Number(delivery[DELIVERY_BATSMAN_RUN]);
            }
            else {
                let ballAndRun = [];
                ballAndRun[0] = 1;
                ballAndRun[1] = Number(delivery[DELIVERY_WIDE_RUN]) + Number(delivery[DELIVERY_NO_BALL_RUN]) + Number(delivery[DELIVERY_BATSMAN_RUN]);
                bowlerData[delivery[DELIVERY_BOWLER]] = ballAndRun;
            }
        }
    });
    let sortedEconomyData = [];
    for (const property in bowlerData) {
        let bowlerName = property;
        let economy = (bowlerData[property][1] * 6) / (bowlerData[property][0]);
        let economyData = [bowlerName, economy];
        sortedEconomyData.push(economyData);
        sortedEconomyData.sort((a, b) => {
            return a[1] - b[1];
        });
    }
    sortedEconomyData.forEach(function (economyData) {
        console.log(economyData[0] + ": " + economyData[1] + "\n");
    });
}

var findExtraRunConcededPerTeamIn2010 = function (matches, deliveries) {
    let extraRunConcededPerTeamin2010 = {};
    const matchIdIn2010 = new Set();
    matches.forEach(function (match) {
        if (match[MATCH_SEASON] == 2010) {
            matchIdIn2010.add(match[MATCH_ID]);
        }
    });
    deliveries.forEach(function (delivery) {
        if (matchIdIn2010.has(delivery[DELIVERY_MATCH_ID])) {
            if (delivery[DELIVERY_BOWLING_TEAM] in extraRunConcededPerTeamin2010) {
                extraRunConcededPerTeamin2010[delivery[DELIVERY_BOWLING_TEAM]] += Number(delivery[DELIVERY_EXTRA_RUN]);
            }
            else {
                extraRunConcededPerTeamin2010[delivery[DELIVERY_BOWLING_TEAM]] = Number(delivery[DELIVERY_EXTRA_RUN]);
            }
        }
    });
    console.log(extraRunConcededPerTeamin2010);
}

findMatchesPlayedPerYear(matches);
findTotalMatchesWonByEachTeam(matches);
findExtraRunConcededPerTeamIn2016(matches, deliveries);
findSortedBowlerEconomyIn2015(matches, deliveries);
findExtraRunConcededPerTeamIn2010(matches, deliveries);